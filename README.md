# Execution
In order to create the needed resources, you must execute the following commands (their respective output is shown)

## Check what changes do we have
```shell
terraform plan -out tfplan.out
```
It is important to output the result to a file. This way we ensure that when applying the changes we take that "state"
as source. If something has changed we will notice.

```log
An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_cloudwatch_log_group.EKS_LogGroup will be created
  + resource "aws_cloudwatch_log_group" "EKS_LogGroup" {
      + arn               = (known after apply)
      + id                = (known after apply)
      + name              = "EKS_Tutorial_LG"
      + retention_in_days = 1
      + tags              = {
          + "Application" = "AWS Tutorial"
          + "Environment" = "production"
        }
    }

  # aws_eks_cluster.aws_eks will be created
  + resource "aws_eks_cluster" "aws_eks" {
      + arn                   = (known after apply)
      + certificate_authority = (known after apply)
      + created_at            = (known after apply)
      + endpoint              = (known after apply)
      + id                    = (known after apply)
      + identity              = (known after apply)
      + name                  = "eks_cluster_test"
      + platform_version      = (known after apply)
      + role_arn              = (known after apply)
      + status                = (known after apply)
      + tags                  = {
          + "Application" = "AWS EKS Tutorial"
          + "Name"        = "EKS_test"
        }
      + version               = (known after apply)

      + kubernetes_network_config {
          + service_ipv4_cidr = (known after apply)
        }

      + vpc_config {
          + cluster_security_group_id = (known after apply)
          + endpoint_private_access   = false
          + endpoint_public_access    = true
          + public_access_cidrs       = (known after apply)
          + subnet_ids                = [
              + "subnet-00e4cc76",
              + "subnet-988db9fc",
            ]
          + vpc_id                    = (known after apply)
        }
    }

  # aws_eks_node_group.node will be created
  + resource "aws_eks_node_group" "node" {
      + ami_type        = (known after apply)
      + arn             = (known after apply)
      + capacity_type   = (known after apply)
      + cluster_name    = "eks_cluster_test"
      + disk_size       = (known after apply)
      + id              = (known after apply)
      + instance_types  = [
          + "T3.micro",
        ]
      + node_group_name = "node_test"
      + node_role_arn   = (known after apply)
      + release_version = (known after apply)
      + resources       = (known after apply)
      + status          = (known after apply)
      + subnet_ids      = [
          + "subnet-00e4cc76",
          + "subnet-988db9fc",
        ]
      + tags            = {
          + "Application" = "AWS EKS Tutorial"
        }
      + version         = (known after apply)

      + scaling_config {
          + desired_size = 1
          + max_size     = 1
          + min_size     = 1
        }
    }

  # aws_iam_role.eks_cluster will be created
  + resource "aws_iam_role" "eks_cluster" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + Service = "eks.amazonaws.com"
                        }
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "eks-cluster-tutorial"
      + path                  = "/"
      + tags                  = {
          + "Application" = "AWS EKS Tutorial"
        }
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # aws_iam_role.eks_nodes will be created
  + resource "aws_iam_role" "eks_nodes" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + Service = "ec2.amazonaws.com"
                        }
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "eks-node-group-test"
      + path                  = "/"
      + tags                  = {
          + "Application" = "AWS EKS Tutorial"
        }
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly will be created
  + resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      + role       = "eks-node-group-test"
    }

  # aws_iam_role_policy_attachment.AmazonEKSClusterPolicy will be created
  + resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
      + role       = "eks-cluster-tutorial"
    }

  # aws_iam_role_policy_attachment.AmazonEKSServicePolicy will be created
  + resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
      + role       = "eks-cluster-tutorial"
    }

  # aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy will be created
  + resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
      + role       = "eks-node-group-test"
    }

  # aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy will be created
  + resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
      + role       = "eks-node-group-test"
    }

Plan: 10 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

This plan was saved to: tfplan.out

To perform exactly these actions, run the following command to apply:
    terraform apply "tfplan.out"
```

## Make those changes effective
```shell
terraform apply "tfplan.out"
```
We must pass the plan output as parameter.
```log
aws_iam_role.eks_nodes: Creating...
aws_iam_role.eks_cluster: Creating...
aws_cloudwatch_log_group.EKS_LogGroup: Creating...
aws_cloudwatch_log_group.EKS_LogGroup: Creation complete after 1s [id=EKS_Tutorial_LG]
aws_iam_role.eks_cluster: Creation complete after 2s [id=eks-cluster-tutorial]
aws_iam_role_policy_attachment.AmazonEKSServicePolicy: Creating...
aws_iam_role_policy_attachment.AmazonEKSClusterPolicy: Creating...
aws_eks_cluster.aws_eks: Creating...
aws_iam_role.eks_nodes: Creation complete after 2s [id=eks-node-group-test]
aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy: Creating...
aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy: Creating...
aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly: Creating...
aws_iam_role_policy_attachment.AmazonEKSServicePolicy: Creation complete after 1s [id=eks-cluster-tutorial-20210322103036447800000002]
aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy: Creation complete after 1s [id=eks-node-group-test-20210322103036450000000003]
aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy: Creation complete after 1s [id=eks-node-group-test-20210322103036444500000001]
aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly: Creation complete after 1s [id=eks-node-group-test-20210322103036500700000005]
aws_iam_role_policy_attachment.AmazonEKSClusterPolicy: Creation complete after 1s [id=eks-cluster-tutorial-20210322103036478400000004]
aws_eks_cluster.aws_eks: Still creating... [10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [1m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [2m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [3m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [4m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [5m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [6m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m10s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m20s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m30s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m40s elapsed]
aws_eks_cluster.aws_eks: Still creating... [7m50s elapsed]
aws_eks_cluster.aws_eks: Still creating... [8m0s elapsed]
aws_eks_cluster.aws_eks: Still creating... [8m10s elapsed]
aws_eks_cluster.aws_eks: Creation complete after 8m20s [id=eks_cluster_test]
aws_eks_node_group.node: Creating...
aws_eks_node_group.node: Creating...
aws_eks_node_group.node: Still creating... [10s elapsed]
aws_eks_node_group.node: Still creating... [20s elapsed]
aws_eks_node_group.node: Still creating... [30s elapsed]
aws_eks_node_group.node: Still creating... [40s elapsed]
aws_eks_node_group.node: Still creating... [50s elapsed]
aws_eks_node_group.node: Still creating... [1m0s elapsed]
aws_eks_node_group.node: Still creating... [1m10s elapsed]
aws_eks_node_group.node: Still creating... [1m20s elapsed]
aws_eks_node_group.node: Still creating... [1m30s elapsed]
aws_eks_node_group.node: Still creating... [1m40s elapsed]
aws_eks_node_group.node: Creation complete after 1m49s [id=eks_cluster_test:node_test]

Apply complete! Resources: 10 added, 0 changed, 0 destroyed.

The state of your infrastructure has been saved to the path
below. This state is required to modify and destroy your
infrastructure, so keep it safe. To inspect the complete state
use the `terraform show` command.

State path: terraform.tfstate
```

## Restore the cloud to the previous state
```shell
terraform destroy
```
It's **very** important to user terraform to destroy all the created resources, because if we left something undeleted,
we can incur in costs and the next time we apply our script it will fail because the resource name is already in use.
```log
An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_cloudwatch_log_group.EKS_LogGroup will be destroyed
  - resource "aws_cloudwatch_log_group" "EKS_LogGroup" {
      - arn               = "arn:aws:logs:eu-west-1:004525482100:log-group:EKS_Tutorial_LG" -> null
      - id                = "EKS_Tutorial_LG" -> null
      - name              = "EKS_Tutorial_LG" -> null
      - retention_in_days = 1 -> null
      - tags              = {
          - "Application" = "AWS Tutorial"
          - "Environment" = "production"
        } -> null
    }

  # aws_eks_cluster.aws_eks will be destroyed
  - resource "aws_eks_cluster" "aws_eks" {
      - arn                       = "arn:aws:eks:eu-west-1:004525482100:cluster/eks_cluster_test" -> null
      - certificate_authority     = [
          - {
              - data = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeE1ETXlNakV3TXpZd01sb1hEVE14TURNeU1ERXdNell3TWxvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTTIvCnIxVXlTS0NOQ3l4MmUxRVdIY2hMQ0ZLeHdTc29IRkhLamExMXYzL3Zjck50ckFqZ1pIQ2J4Nm5QR0RaUklHMTkKdUFBVy9wa1VJS242bHJ1bVdXSGNXbXdLNzlqYzZZNW1zU3NHd0VNVjVOcTczYUgxVWNTM3JxaW9NblBWOElFMApvbUIzbEJTUUsrSnM5cjdjNFBIRnFlclpzeUNqdFdZL0EyLzdRLzYxQlQ1c3dKQWxsN2V4eWlQYnR1MkswSEhWCkczYUxCdHJobFk2aVB2RVZqcEY4Q3ZKS2NDc0c3VHNsZ1dqcy8xK1RiS0VFckl2cVdUaUNEVDVjNjFjdkRjQ3UKZzYybDdaV0ZiUFEyWGhwbDZwYmRONzh3UkNGUXF4RVlSTk9RNW1Fem9ScXFaSFdXa2g1bWx6SUxGQVJQY0hjeApySlkrcitIVVF1RndSekVyNTRVQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFLeEQvNVBnNGd2UzhSbElVVGI5NDNUUGtYWFkKNmVDck8zdkk1ZUx6Skg3dnZYOWFWWDVSeUM2TmhicGt5eDBncTRXY0xmcC9rTm1YZjBKS0IrcUREeXp3eUIxcgpycnQvOVlxaHRqeGNnVm02U25ZcEEyd2pLanZGQVlFSzZhcVNNc2psaEZRVmZSV3FIRyt1eHNTLzV6b3dLRXArClo2WFhnUVI2Wm04L0R5bnpkTjRyMjZUYzNEbDJVLzdMS25OOXkwUjc3aHNWeG5RVVJjUTJWN2N1a3dPcG43Y0IKLzR5R0VmK0xpLzlIU2I2M0ZrVmJJWEJOR3QxSG5OYzI1SGt3NVBBZ3FjM29JSEE2enFvYjY1RFk4bXhQakdINgp5Y3hpV1haVWtyMTFKZ0tLN09Nc3dLanpycDZvdTdUeE00S1lTNUt0MTN6VXhtNGY0ZW8yWXpzZEppRT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo="
            },
        ] -> null
      - created_at                = "2021-03-22 10:30:37.183 +0000 UTC" -> null
      - enabled_cluster_log_types = [] -> null
      - endpoint                  = "https://E67F909C24084EB5736F747CB9A6A4C8.gr7.eu-west-1.eks.amazonaws.com" -> null
      - id                        = "eks_cluster_test" -> null
      - identity                  = [
          - {
              - oidc = [
                  - {
                      - issuer = "https://oidc.eks.eu-west-1.amazonaws.com/id/E67F909C24084EB5736F747CB9A6A4C8"
                    },
                ]
            },
        ] -> null
      - name                      = "eks_cluster_test" -> null
      - platform_version          = "eks.4" -> null
      - role_arn                  = "arn:aws:iam::004525482100:role/eks-cluster-tutorial" -> null
      - status                    = "ACTIVE" -> null
      - tags                      = {
          - "Application" = "AWS EKS Tutorial"
          - "Name"        = "EKS_test"
        } -> null
      - version                   = "1.18" -> null

      - kubernetes_network_config {
          - service_ipv4_cidr = "172.20.0.0/16" -> null
        }

      - vpc_config {
          - cluster_security_group_id = "sg-0c382a30532537750" -> null
          - endpoint_private_access   = false -> null
          - endpoint_public_access    = true -> null
          - public_access_cidrs       = [
              - "0.0.0.0/0",
            ] -> null
          - security_group_ids        = [] -> null
          - subnet_ids                = [
              - "subnet-00e4cc76",
              - "subnet-988db9fc",
            ] -> null
          - vpc_id                    = "vpc-081b3e6c" -> null
        }
    }

  # aws_eks_node_group.node will be destroyed
  - resource "aws_eks_node_group" "node" {
      - ami_type        = "AL2_x86_64" -> null
      - arn             = "arn:aws:eks:eu-west-1:004525482100:nodegroup/eks_cluster_test/node_test/80bc2cc2-9c51-fd9a-63fd-99a2fa22940d" -> null
      - capacity_type   = "ON_DEMAND" -> null
      - cluster_name    = "eks_cluster_test" -> null
      - disk_size       = 20 -> null
      - id              = "eks_cluster_test:node_test" -> null
      - instance_types  = [
          - "t3.micro",
        ] -> null
      - node_group_name = "node_test" -> null
      - node_role_arn   = "arn:aws:iam::004525482100:role/eks-node-group-test" -> null
      - release_version = "1.18.9-20210310" -> null
      - resources       = [
          - {
              - autoscaling_groups              = [
                  - {
                      - name = "eks-80bc2cc2-9c51-fd9a-63fd-99a2fa22940d"
                    },
                ]
              - remote_access_security_group_id = ""
            },
        ] -> null
      - status          = "ACTIVE" -> null
      - subnet_ids      = [
          - "subnet-00e4cc76",
          - "subnet-988db9fc",
        ] -> null
      - tags            = {
          - "Application" = "AWS EKS Tutorial"
        } -> null
      - version         = "1.18" -> null

      - scaling_config {
          - desired_size = 1 -> null
          - max_size     = 1 -> null
          - min_size     = 1 -> null
        }
    }

  # aws_iam_role.eks_cluster will be destroyed
  - resource "aws_iam_role" "eks_cluster" {
      - arn                   = "arn:aws:iam::004525482100:role/eks-cluster-tutorial" -> null
      - assume_role_policy    = jsonencode(
            {
              - Statement = [
                  - {
                      - Action    = "sts:AssumeRole"
                      - Effect    = "Allow"
                      - Principal = {
                          - Service = "eks.amazonaws.com"
                        }
                    },
                ]
              - Version   = "2012-10-17"
            }
        ) -> null
      - create_date           = "2021-03-22T10:30:34Z" -> null
      - force_detach_policies = false -> null
      - id                    = "eks-cluster-tutorial" -> null
      - managed_policy_arns   = [
          - "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
          - "arn:aws:iam::aws:policy/AmazonEKSServicePolicy",
        ] -> null
      - max_session_duration  = 3600 -> null
      - name                  = "eks-cluster-tutorial" -> null
      - path                  = "/" -> null
      - tags                  = {
          - "Application" = "AWS EKS Tutorial"
        } -> null
      - unique_id             = "AROAQCDN5MB2HH4CPWR5C" -> null

      - inline_policy {}
    }

  # aws_iam_role.eks_nodes will be destroyed
  - resource "aws_iam_role" "eks_nodes" {
      - arn                   = "arn:aws:iam::004525482100:role/eks-node-group-test" -> null
      - assume_role_policy    = jsonencode(
            {
              - Statement = [
                  - {
                      - Action    = "sts:AssumeRole"
                      - Effect    = "Allow"
                      - Principal = {
                          - Service = "ec2.amazonaws.com"
                        }
                    },
                ]
              - Version   = "2012-10-17"
            }
        ) -> null
      - create_date           = "2021-03-22T10:30:34Z" -> null
      - force_detach_policies = false -> null
      - id                    = "eks-node-group-test" -> null
      - managed_policy_arns   = [
          - "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
          - "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
          - "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
        ] -> null
      - max_session_duration  = 3600 -> null
      - name                  = "eks-node-group-test" -> null
      - path                  = "/" -> null
      - tags                  = {
          - "Application" = "AWS EKS Tutorial"
        } -> null
      - unique_id             = "AROAQCDN5MB2LZHUUYT3G" -> null

      - inline_policy {}
    }

  # aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly will be destroyed
  - resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
      - id         = "eks-node-group-test-20210322103036500700000005" -> null
      - policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly" -> null
      - role       = "eks-node-group-test" -> null
    }

  # aws_iam_role_policy_attachment.AmazonEKSClusterPolicy will be destroyed
  - resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
      - id         = "eks-cluster-tutorial-20210322103036478400000004" -> null
      - policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy" -> null
      - role       = "eks-cluster-tutorial" -> null
    }

  # aws_iam_role_policy_attachment.AmazonEKSServicePolicy will be destroyed
  - resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
      - id         = "eks-cluster-tutorial-20210322103036447800000002" -> null
      - policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy" -> null
      - role       = "eks-cluster-tutorial" -> null
    }

  # aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy will be destroyed
  - resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
      - id         = "eks-node-group-test-20210322103036450000000003" -> null
      - policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy" -> null
      - role       = "eks-node-group-test" -> null
    }

  # aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy will be destroyed
  - resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
      - id         = "eks-node-group-test-20210322103036444500000001" -> null
      - policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy" -> null
      - role       = "eks-node-group-test" -> null
    }

Plan: 0 to add, 0 to change, 10 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

aws_iam_role_policy_attachment.AmazonEKSServicePolicy: Destroying... [id=eks-cluster-tutorial-20210322103036447800000002]
aws_cloudwatch_log_group.EKS_LogGroup: Destroying... [id=EKS_Tutorial_LG]
aws_iam_role_policy_attachment.AmazonEKSClusterPolicy: Destroying... [id=eks-cluster-tutorial-20210322103036478400000004]
aws_eks_node_group.node: Destroying... [id=eks_cluster_test:node_test]
aws_cloudwatch_log_group.EKS_LogGroup: Destruction complete after 1s
aws_iam_role_policy_attachment.AmazonEKSServicePolicy: Destruction complete after 1s
aws_iam_role_policy_attachment.AmazonEKSClusterPolicy: Destruction complete after 1s
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 10s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 20s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 30s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 40s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 50s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m0s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m10s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m20s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m30s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m40s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 1m50s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 2m0s elapsed]
aws_eks_node_group.node: Still destroying... [id=eks_cluster_test:node_test, 2m10s elapsed]
aws_eks_node_group.node: Destruction complete after 2m19s
aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy: Destroying... [id=eks-node-group-test-20210322103036444500000001]
aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly: Destroying... [id=eks-node-group-test-20210322103036500700000005]
aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy: Destroying... [id=eks-node-group-test-20210322103036450000000003]
aws_eks_cluster.aws_eks: Destroying... [id=eks_cluster_test]
aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy: Destruction complete after 0s
aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy: Destruction complete after 0s
aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly: Destruction complete after 1s
aws_iam_role.eks_nodes: Destroying... [id=eks-node-group-test]
aws_iam_role.eks_nodes: Destruction complete after 1s
aws_eks_cluster.aws_eks: Still destroying... [id=eks_cluster_test, 10s elapsed]
aws_eks_cluster.aws_eks: Still destroying... [id=eks_cluster_test, 20s elapsed]
aws_eks_cluster.aws_eks: Still destroying... [id=eks_cluster_test, 30s elapsed]
aws_eks_cluster.aws_eks: Still destroying... [id=eks_cluster_test, 40s elapsed]
aws_eks_cluster.aws_eks: Destruction complete after 46s
aws_iam_role.eks_cluster: Destroying... [id=eks-cluster-tutorial]
aws_iam_role.eks_cluster: Destruction complete after 2s

Destroy complete! Resources: 10 destroyed.
```