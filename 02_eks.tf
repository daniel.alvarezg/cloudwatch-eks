// Configuring the provider allows easy usage when multiple accounts managed
// and avoids mistakes.
provider "aws" {
  region = "eu-west-1"
  // Profile defined in AWS Credentials file
  profile = "Autentia"
}

resource "aws_iam_role" "eks_cluster" {
  name = "eks-cluster-tutorial"

  // As the console output is JSON formatted this allows to paste output directly
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "eks.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })

  tags = {
    Application = "AWS EKS Tutorial"
  }
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role = aws_iam_role.eks_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role = aws_iam_role.eks_cluster.name
}

resource "aws_eks_cluster" "aws_eks" {
  name = "eks_cluster_test"
  role_arn = aws_iam_role.eks_cluster.arn

  version = "1.19" // To trigger an update to 1.19

  vpc_config {
    subnet_ids = [
      "subnet-00e4cc76",
      "subnet-988db9fc"
    ]
  }

  tags = {
    Name = "EKS_test",
    Application = "AWS EKS Tutorial"
  }
}

resource "aws_iam_role" "eks_nodes" {
  name = "eks-node-group-test"

  tags = {
    Application = "AWS EKS Tutorial"
  }

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "CloudWatchAgentServerPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  role = aws_iam_role.eks_nodes.name
}

resource "aws_eks_node_group" "node" {
  cluster_name = aws_eks_cluster.aws_eks.name
  node_group_name = "node_test"
  node_role_arn = aws_iam_role.eks_nodes.arn
  subnet_ids = [
    "subnet-00e4cc76",
    "subnet-988db9fc"
  ]

//  Fix the instance type to a free tier one
  instance_types = ["t3.small"]

  scaling_config {
    desired_size = 2
    max_size = 2
    min_size = 1
  }

  // Avoid Terraform detecting changes on desired_size due to scaling policies
  // If our application has scaled up we do not change current instances ;)
  # lifecycle {
  #   ignore_changes = [scaling_config[0].desired_size]
  # }

  tags = {
    Application = "AWS EKS Tutorial"
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.CloudWatchAgentServerPolicy,
  ]
}